var AffairsDB = (function(websql){
	
	var  db;
	var DB_NAME = "MyAffairsDB"
	var DB_VERSION = "0.00"
	
	var SQL_TABLE = 'DROP TABLE IF EXISTS affaires;CREATE TABLE affaires (guid TEXT PRIMARY KEY, title TEXT,time_begin TEXT,time_end TEXT,image TEXT,description TEXT);';
	var SQL_INSERT = 'INSERT INTO affaires (guid,title,time_begin,time_end,image,description) VALUES(?,?,?,?,?,?);';
	var SQL_SELECT_ALL = 'SELECT * FROM affaires;';
	var SQL_UPDATE = 'UPDATE affaires SET image = ? WHERE guid = ?';
	var SQL_DELETE = 'DELETE FROM affaires';
	
	function printErr(err){
		console.log("Error:"+err.message)
//		mui.toast("Error:"+err.message);
	};
	
	return {
		ready:function(successCallback, errorCallback){
			websql.openDatabase(DB_NAME, DB_NAME, 3 * 1024 * 1024);
			if (websql.database.version === '') {
				websql.changeVersion('', DB_VERSION, SQL_TABLE, function() {
					successCallback && successCallback(true);
				}, function(error, failingQuery) {
					printErr(error)
					errorCallback && errorCallback(error, failingQuery);
				});
			} else {
				successCallback && successCallback(false);
			}
		},
		insert:function(affair, successCallback, errorCallback){
			websql.process([{
				"sql": SQL_INSERT,
				"data": [affair.guid, affair.title, affair.time_begin, affair.time_end, affair.image, affair.description],
			}], function(tx, results) {
			successCallback && successCallback(true);
		    }, function(error, failingQuery) {
			printErr(error)
			errorCallback && errorCallback(error, failingQuery);
		});

			
		},
		delete:function(){},
		queryall:function(successCallback, errorCallback){
			websql.process([{
				"sql": SQL_SELECT_ALL,
				}], 
				function(tx, results) {
					successCallback(results.rows);
				}, 
				function(error, failingQuery) {
					printErr(error)
					errorCallback && errorCallback(error, failingQuery);
				});
		},
	}
	
	
}(html5sql));


(function(DB){
	DB.ready(function(){
		DB.insert({
			guid: 1,
			title: "title1",
		})
		DB.insert({
			guid: 2,
			title: "title2",
		})
	})
}(AffairsDB));
